<style>
	.footer {
		bottom: 0;
		width: 100%;
		height: 60px;
		background-color: #f5f5f5;
	}
	
	.container .text-muted {
		margin: 20px 0;
	}
	
	html, body {
		height: 100%;
	}
	
	nav {
		margin-bottom: 0px!important;
	}
	
</style>

<footer class="footer">
  <div class="container">
	<p class="text-muted">Josh Holzbach July 2017.</p>
  </div>
</footer>