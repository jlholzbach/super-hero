<input type="hidden" id="resize" name="resize" value="Y" />

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="index.php">
			Super Hero Fan Club
		  </a>
		</div>
		
		<!-- class="sr-only"> -->
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li>
				<a href="entertainment.php">Archive of Entertainment</a>
			</li>
			<li>
				<a href="#">Hall of Trivia</a>
			</li>
			<li>
				<a href="#">Heroic Quotes</a>
			</li>
			<li>
				<a href="#">Registration</a>
			</li>
			<li>
				<a href="slideshow.php">Slideshow of Heroes</a>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
		
	</div>	
</nav>